class Game {
  constructor(cell, results) {
    this.cell = document.querySelectorAll(cell);
    this.results = document.querySelector(results);
    this.step = 0;
    this.bindEvents();
  }

  bindEvents() {
    this.cell.forEach(cell => cell.addEventListener('click', this.move.bind(this)));
  }

  move({ target }) {
    if (target.innerHTML || this.results.innerHTML) return;

    !(this.step % 2) ? target.innerHTML = 'X' : target.innerHTML = 'O';
    target.classList.add('cell--active');

    this.step++;
    const cell = (number) => this.cell[number].innerHTML;
    const check = (first, second, third) => {
      if (cell(first) === cell(second) && cell(first) === cell(third) && cell(first)) {
        this.results.innerHTML = `Победили - ${cell(first)}`;
      }
    }
    const vertically  = (first) => {
      check(first, first + 3, first + 6)
    };
    const horizontally = (first) => {
      check(first, first + 1, first + 2)
    };
    const diagonal = () => {
      check(0, 4, 8);
      check(2, 4, 6);
    }

    [0, 1, 2].forEach(number => vertically(number));
    [0, 3, 6].forEach(number => horizontally(number));
    diagonal();
  }
}

new Game('.j-cell', '.j-result');

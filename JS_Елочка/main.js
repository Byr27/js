const max = 55;

const star = '*';
const br = '\n';
const space = ' ';

let tree = ``;
let tree1 = ``;

for (let i = 0; i < max -1; i++) {
  tree += space;
}
tree += star;
tree += br;

for (let i = 0; i < max - 1; i++) {
  const spaceAmount = max - i - 2;
  const starAmount = (1 + i) * 2 - 1;

  for (let j = 0; j < spaceAmount ; j++) {
    tree += space;
  }

  tree += star;

  for ( let h = 0; h < starAmount; h++ ) {
    if ( i === max - 2) {
      tree += star;
    } else {
      tree += space;
    }
  }

  tree += star
  tree += br;
}



for (let i = 0; i < max; i++) {
  const spaceAmount = max - i - 1;
  const starAmount = 1 + i*2;
  for (let j = 0; j < spaceAmount; j++) {
    tree1 += space;
  }

  for ( let h = 0; h < starAmount; h++ ) {
    tree1 += star;
  }

  tree1 += br;
}

console.log(tree);
console.log(tree1);

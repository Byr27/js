class Login {

  constructor(formSelector) {
    this.form = document.body.querySelector(formSelector);
    this.bindEvents()
  }

  bindEvents() {
    const inputs = Array.from(this.form.querySelectorAll('.j-input'));

    inputs.forEach(input => input.addEventListener('input', this.validation))
  }

  validation({ target }) {
    const { value } = target;
    const { validation } = target.dataset;
    const mainRules = validation.split('|');
    const rules = mainRules.map(rule => rule.split(':'))
    const error = target.closest('.j-wrapper').querySelector('.j-error');

    error.innerText =''

    rules.forEach(rule =>  {
      if(rule.includes('required') && !value) {
        error.innerText = `поле не должно быть пустым`
      }

      if(rule.includes('min_length') && value.length < rule[1] && value) {
        error.innerText = `поле должно быть длиннее ${rule[1]} символов`
      }

      if(rule.includes('max_length') && value.length >=  rule[1]) {
        error.innerText = `поле должно быть мение ${rule[1]} символов`
      }
    })
  }
}

new Login('.j-registration');

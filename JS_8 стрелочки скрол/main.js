const Top = document.querySelector('.top');
const Down = document.querySelector('.down');
let currentPosition;
Top.addEventListener('click', toTop);
Down.addEventListener('click', toBack);
window.addEventListener('scroll', togleButton);

function togleButton(event) {
  if (window.pageYOffset > document.documentElement.clientHeight) {
    Top.style.display = 'block';
  } else {
    Top.style.display = ''
  };
  if (window.pageYOffset > 1) Down.style.display = '';
}

function toTop(event) {
  currentPosition = window.pageYOffset;
  window.scrollTo(0,0);
  Down.style.display = 'block';
};

function toBack(event) {
  window.scrollTo(0, currentPosition);
}

// const test = () => {
//   return fetch('https://reqres.in/api/users?page=3')
// }

// (async ()=> {
//   const test2 = await test()
//   console.log(test2);
// })();



const func = (a, func2) => {
  return (b) => (c) => (e) => {
    return func2(a + b + c + e);
  }
}

func(1, (result) => {
  console.log(result);
})(2)(4)(1);


const test = () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve();
      console.log(2);
    }, 2000)
  });
}

(async()=> {
  await test();
  console.log(3);
})()

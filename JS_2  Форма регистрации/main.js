  class Login {
    constructor(formSelector) {
      this.form = document.body.querySelector(formSelector);
      this.bindEvents();
    }

    bindEvents() {
      this.form.addEventListener('submit', this.sendData.bind(this));
      this.form.querySelectorAll('.j-input').forEach((item) => {
        item.addEventListener('input', ({ target }) => this.validate(target));
      });
    }

    validate(target) {
      const { value } = target;
      const { validation } = target.dataset;
      const mainRules = validation.split('|');
      const rules = mainRules.map(rule => rule.split(':'))
      const error = target.closest('.j-wrapper').querySelector('.j-error');

      error.innerText =''

      return rules.reduce((isValid, rule) =>  {
        if(rule.includes('required') && !value) {
          error.innerText = `поле не должно быть пустым`;
          isValid = false;
        }

        if(rule.includes('min_length') && value.length < rule[1] && value) {
          error.innerText = `поле должно быть длиннее ${rule[1]} символов`;
          isValid = false;
        }

        if(rule.includes('max_length') && value.length >=  rule[1]) {
          error.innerText = `поле должно быть мение ${rule[1]} символов`;
          isValid = false;
        }

        return isValid;
      }, true);
    }

    sendData(event) {
      event.preventDefault();

      const inputs = Array.from(this.form.querySelectorAll('.j-input'));
      const uploader = this.form.querySelector('.j-file');
      const data = new FormData();
      const isValidInputs = inputs.reduce((isValid, input) => {
        const isValidInput = this.validate(input);

        if (!isValid) {
          return isValid;
        }

        return isValidInput;
      }, true);

      if (!isValidInputs) return;

      inputs.forEach((item) => {
        data.append(item.name, JSON.stringify(item.value));
      });
      data.append('photo', uploader.files[0]);

      fetch('https://reqres.in/api/register', {
        method: 'POST',
        body: data
      })
    }
}

new Login('.j-registration');




class Calculate {
  constructor(formSelector) {
    this.form = document.body.querySelector(formSelector);

    this.bindEvents();
  }

  bindEvents() {
    this.form.addEventListener("submit", this.submit);
  }

  submit(event) {
    event.preventDefault();

    const inputs = Array.from(event.target.querySelectorAll(".j-input"));
    const lastInput = inputs[inputs.length-1];
    lastInput.setAttribute('disabled', 'disabled');

    inputs.forEach(input => {
      if(input.value) {
        lastInput.value = '';
      }
    });

    const result = inputs.reduce((result, input) => {
      return (result += Number(input.value) || 0);
    }, 0);
    inputs.forEach(input => input.value = '');
    lastInput.value = result
  }
}

new Calculate(".j-calculate");

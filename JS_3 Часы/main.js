
  const div = document.createElement('div')
  document.body.appendChild(div);

const timer = () => {
  const date = new Date();
  let hours = date.getHours();
    if (hours < 10) hours = '0' + hours;
  let min = date.getMinutes();
    if (min < 10) min = '0' + min;
  let sec = date.getSeconds();
    if (sec < 10) sec = '0' + sec;
  div.innerHTML = `${hours}:${min}:${sec}`;
}

timer();
setInterval(timer, 1000);

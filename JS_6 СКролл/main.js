const block = document.querySelector('.block');
let pos = 1;

block.addEventListener('wheel', (e) => {
  e.preventDefault()

  var delta = e.deltaY;

  pos += -delta / 1000;

  block.style.transform =    'scale('+pos+')';
  console.log(block.style.top);

})

var elem = document.getElementById('container');

elem.addEventListener("wheel", onWheel);

function onWheel(e) {
  e.preventDefault();

  var delta = e.deltaY;

  var info = document.getElementById('delta');
  console.log(delta);
  info.innerHTML = +info.innerHTML + delta;
}

document.onwheel = function(e) {
  if (e.target.tagName != 'TEXTAREA') return;
  var area = e.target;

  var delta = e.deltaY || e.detail || e.wheelDelta;

  if (delta < 0 && area.scrollTop == 0) {
    e.preventDefault();
  }

    // console.log(area.scrollTop);
    // console.log(delta);

  if (delta > 0 && area.scrollHeight - area.clientHeight - area.scrollTop <= 1) {
    e.preventDefault();
  }

  console.log(area.scrollHeight);
  console.log(area.clientHeight);
  console.log(area.scrollTop);
};

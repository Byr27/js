const img = document.querySelector('#avatar');

window.addEventListener('scroll', showAvatar);

function showAvatar(event) {
  if(window.pageYOffset > 300)  {
    img.style.position = 'fixed';
    img.style.top = '0';
    img.style.left = '0';
  } else {
    img.style.position = '';
    img.style.top = '';
    img.style.left = '';
  }
}

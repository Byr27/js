class Global {
  constructor() {
    this.bindEvents();
  }

  bindEvents() {
    document.querySelector('.j-file-input').addEventListener('change', this.setFileName);
  }

  setFileName({ target }) {
    const label = target.closest('.j-file-label');
    const name = label.querySelector('.j-file-name');
    const output = document.querySelector('.j-output');

    output.innerHTML = ' ';
    name.innerText = target.files.length === 0
      ? 'Выбрать файл'
      : target.files[0].name;
  }
}

new Global();

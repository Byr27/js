class Uploader {
  constructor(formSelector, outputSelector, wrapperSelector) {
    this.form = document.querySelector(formSelector);
    this.output = document.querySelector(outputSelector);
    this.wrapper = document.querySelector(wrapperSelector);

    this.bindEvents();
  }

  bindEvents() {
    this.form.addEventListener('submit', this.outputInfo.bind(this));
    document.addEventListener('dragenter', this.toggleActive.bind(this));
    document.addEventListener('dragleave', this.toggleActive.bind(this));
    document.addEventListener('drop', this.removeActive.bind(this));
  }

  toggleActive() {
    this.wrapper.classList.toggle('uploader-wrapper--active');
    this.form.querySelector('input[type="file"]').classList.toggle('uploader__input--active');
  }

  removeActive() {
    this.form.querySelector('input[type="file"]').classList.remove('uploader__input--active');
    this.wrapper.classList.remove('uploader-wrapper--active');
  }

  outputInfo(event) {
    event.preventDefault();

    const { files } = event.target.querySelector('input[type="file"]');

    const { size, type } = files[0];
    const getFileType = (type) => {
      switch (type) {
        case 'image/jpeg' || 'image/png':
          return `Картинка: ${type.split('/')[1].toUpperCase()}`;
        case 'text/javascript':
          return 'JavaScript'
        default:
          return 'Формат неопознан';
      }
    };

    this.output.innerHTML = `Размер файла: ${ size > (1024*1000)
      ? ((size / (1024*1000)).toFixed(2) + ' MБайт')
      : size > 1024
        ? (size / 1024).toFixed() + ' КБайт'
        : size + ' Байт'} ${getFileType(type)}`;
  }
}

new Uploader('.j-form', '.j-output', '.j-upload-wrapper');
